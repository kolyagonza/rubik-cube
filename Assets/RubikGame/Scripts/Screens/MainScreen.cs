using System.Collections;
using System.Collections.Generic;
using Navigation;
using RubikGame;
using RubikGame.Core;
using UnityEngine;
using UnityEngine.UI;
using Zenject;

public class MainScreen : BaseScreen
{
    [SerializeField]
    private Button continueButton;

    private PlayerModel playerModel;

    [Inject]
    private void Init(PlayerModel playerModel, GameStateResolver gameStateResolver)
    {
        this.playerModel = playerModel;
        gameStateResolver.CurrentState = GameStateResolver.GameState.Menu;
    }

    public override void OnScreenLoading()
    {
        continueButton.interactable = playerModel.HasSaveFile();
    }

    public void OnNewGameClick()
    {
        Navigation.Push<ChooseGameModeScreen>();
    }

    public void OnContinueClick()
    {
        playerModel.ContinueGame();
        Navigation.Pop();
        Navigation.Push<GameScreen>();
    }
}
