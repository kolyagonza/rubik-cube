using System;
using System.Collections;
using System.Collections.Generic;
using DG.Tweening;
using Navigation;
using RubikGame;
using RubikGame.Core;
using RubikGame.Screens;
using TMPro;
using UnityEngine;
using UnityEngine.UI;
using Zenject;
using Random = UnityEngine.Random;

public class GameScreen : BaseScreen
{
    private PlayerModel playerModel;
    private CubeGenerator cubeGenerator;
    private CubeMover cubeMover;
    private GameStateResolver gameStateResolver;
    private WinConditionChecker winConditionChecker;
    private CubeProcessor cubeProcessor;

    [SerializeField] 
    private Button undoButton;
    [SerializeField]
    private TextMeshProUGUI timerText;
    [SerializeField] 
    private TextMeshProUGUI movesCount;
    [SerializeField]
    private GameObject timerTextContainer;
    [SerializeField] 
    private GameObject movesTextContainer;

    private Vector3 startContainerPosition;
    
    [Inject]
    private void Init(PlayerModel playerModel, 
        CubeGenerator cubeGenerator, 
        CubeMover cubeMover,
        GameStateResolver gameStateResolver,
        WinConditionChecker winConditionChecker, 
        CubeProcessor cubeProcessor)
    {
        this.playerModel = playerModel;
        this.cubeGenerator = cubeGenerator;
        this.cubeMover = cubeMover;
        this.gameStateResolver = gameStateResolver;
        this.winConditionChecker = winConditionChecker;
        this.cubeProcessor = cubeProcessor;
        cubeMover.UndoStateChanged += CubeMoverOnUndoStateChanged;
        playerModel.PlayerSaveChanged += PlayerModelOnPlayerSaveChanged;
        winConditionChecker.GameWon += WinConditionCheckerOnGameWon;
    }

    private void WinConditionCheckerOnGameWon()
    {
        gameStateResolver.CurrentState = GameStateResolver.GameState.Won;
        StartCoroutine(StartWinAnimation());
    }

    private IEnumerator StartWinAnimation()
    {
        startContainerPosition = cubeGenerator.CubesContainer.transform.position;
        while (cubeMover.IsBusy)
        {
            yield return null;
        }
        
        var sequence = DOTween.Sequence();
        sequence.Append(cubeGenerator.CubesContainer.transform
                .DOLocalRotate(new Vector3(0, 0, 360), 0.8f, RotateMode.FastBeyond360).SetRelative(true).SetEase(Ease.InElastic))
            .Append(cubeGenerator.CubesContainer.transform.DOScale(new Vector3(1.2f, 1.2f, 1.2f), 1.2f).SetEase(Ease.InElastic))
            .Append(cubeGenerator.CubesContainer.transform.DOMove(new Vector3(6f, 6f, 6f), 1.5f).SetEase(Ease.InElastic))
            .OnComplete(OnWinAnimationComplete);
        sequence.Play();
    }

    private void OnWinAnimationComplete()
    {
        cubeGenerator.CubesContainer.transform.position = startContainerPosition;
        Navigation.PopToRoot();
        Navigation.Push<WinScreen>();
    }

    private void PlayerModelOnPlayerSaveChanged(PlayerModel.PlayerSave playerSave)
    {
        if (playerModel.ShowTimer)
        {
            timerText.text = playerSave.TimeSpend.ToString();
        }

        if (playerModel.ShowMoves)
        {
            movesCount.text = playerSave.MovesCount.ToString();
        }
    }

    private void CubeMoverOnUndoStateChanged(bool ableToUndo)
    {
        undoButton.interactable = ableToUndo;
    }

    public override void OnScreenLoading()
    {
        List<PieceInfo> oldData = null;
        if (playerModel.ContinueFlag)
        {
            oldData = playerModel.ContinueData;
        }
        cubeGenerator.GenerateCube(playerModel.GameMode, oldData);
        if (!playerModel.ContinueFlag)
        {
            GenerateNewGame();
        }
        else
        {
            gameStateResolver.CurrentState = GameStateResolver.GameState.Game;
        }
        
        playerModel.GetCurrentTimeAndMovesValues(out var time, out var moves);
        if (playerModel.ShowTimer)
        {
            timerText.text = time.ToString();
        }
        else
        {
            timerTextContainer.SetActive(false);
        }

        if (playerModel.ShowMoves)
        {
            movesCount.text = moves.ToString();
        }
        else
        {
            movesTextContainer.SetActive(false);
        }
        
    }

    public override void Resume()
    {
        gameStateResolver.CurrentState = GameStateResolver.GameState.Game;
    }

    public void OnMenuButtonClick()
    {
        var screen = (GameMenuScreen)Navigation.Push<GameMenuScreen>();
        screen.OnMovesToggleValueChanged += OnMovesToggleValueChanged;
        screen.OnTimerToggleValueChanged += OnTimerToggleValueChanged;
        screen.Closed += UnsubscribeFromGameMenuEvents;
    }

    private void OnMovesToggleValueChanged(bool toggleValue)
    {
        movesTextContainer.SetActive(toggleValue);
        playerModel.ShowMoves = toggleValue;
    }
    
    private void OnTimerToggleValueChanged(bool toggleValue)
    {
        timerTextContainer.SetActive(toggleValue);
        playerModel.ShowTimer = toggleValue;
    }

    private void UnsubscribeFromGameMenuEvents(GameMenuScreen screen)
    {
        screen.OnMovesToggleValueChanged -= OnMovesToggleValueChanged;
        screen.OnTimerToggleValueChanged -= OnTimerToggleValueChanged;
        screen.Closed -= UnsubscribeFromGameMenuEvents;
    }

    public override void OnScreenClose()
    {
        gameStateResolver.CurrentState = GameStateResolver.GameState.Menu;
        cubeGenerator.DestroyCurrentCube();
        cubeMover.UndoStateChanged -= CubeMoverOnUndoStateChanged;
        playerModel.PlayerSaveChanged -= PlayerModelOnPlayerSaveChanged;
        winConditionChecker.GameWon -= WinConditionCheckerOnGameWon;
    }

    public void OnUndoClick()
    {
        cubeMover.UndoCommand();
    }
    
    private void GenerateNewGame()
    {
        var randomMovesCount = Random.Range(10, 15);
        var maxCubeIndex = (int)Math.Pow((int)playerModel.GameMode, 3);
        for (var i = 0; i < randomMovesCount; i++)
        {
            var randomCommandNumber = Random.Range(0, 4);
            var randomIndex = Random.Range(0, maxCubeIndex);
            var command = GenerateNewCommand(randomCommandNumber, randomIndex);
            if (i == randomMovesCount - 1)
            {
                command.CommandDone += StartNewGameLastCommandDone;
            }
        }
    }

    private void StartNewGameLastCommandDone(ICommand command)
    {
        command.CommandDone -= StartNewGameLastCommandDone;
        gameStateResolver.CurrentState = GameStateResolver.GameState.Game;
    }

    private ICommand GenerateNewCommand(int commandIndex, int cubeIndex)
    {
        ICommand command = null;
        switch (commandIndex)
        {
            case 0:
                command =
                    new HorizontalClockwiseRotateCommand(cubeProcessor, cubeGenerator.CubesContainer, cubeIndex);
                break;
            case 1:
                command =
                    new HorizontalCounterClockwiseRotateCommand(cubeProcessor, cubeGenerator.CubesContainer, cubeIndex);
                break;
            case 2:
                command =
                    new VerticalClockwiseRotateCommand(cubeProcessor, cubeGenerator.CubesContainer, cubeIndex);
                break;
            case 3:
                command =
                    new VerticalCounterClockwiseRotateCommand(cubeProcessor, cubeGenerator.CubesContainer, cubeIndex);
                break;
        }

        command.Record = false;
        cubeMover.AddCommand(command);
        return command;
    }
}
