using Navigation;
using TMPro;
using UnityEngine;
using Zenject;

namespace RubikGame.Screens
{
    public class WinScreen : BaseScreen
    {
        [SerializeField] private TextMeshProUGUI movesCount;
        [SerializeField] private TextMeshProUGUI timeCount;

        private PlayerModel playerModel;

        [Inject]
        private void Init(PlayerModel playerModel)
        {
            this.playerModel = playerModel;
        }

        public override void OnScreenLoading()
        {
            playerModel.GetCurrentTimeAndMovesValues(out var time, out var moves);
            movesCount.text = string.Format(movesCount.text, moves);
            timeCount.text = string.Format(timeCount.text, time);
        }

        public void MainMenuClick()
        {
            playerModel.StartNewSave();
            Navigation.Pop();
            Navigation.Push<MainScreen>();
        }

        public void NewGameClick()
        {
            Navigation.Push<ChooseGameModeScreen>();
        }
    }
}