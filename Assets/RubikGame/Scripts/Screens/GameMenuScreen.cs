using System;
using Navigation;
using UnityEngine;
using UnityEngine.UI;
using Zenject;

namespace RubikGame.Screens
{
    public class GameMenuScreen : BaseScreen
    {
        public event Action<bool> OnTimerToggleValueChanged;
        public event Action<bool> OnMovesToggleValueChanged;
        public event Action<GameMenuScreen> Closed;

        [SerializeField] private Toggle timerToggle;
        [SerializeField] private Toggle movesToggle;

        [Inject]
        private void Init(PlayerModel playerModel, GameStateResolver gameStateResolver)
        {
            timerToggle.isOn = playerModel.ShowTimer;
            movesToggle.isOn = playerModel.ShowMoves;
            gameStateResolver.CurrentState = GameStateResolver.GameState.Pause;
        }

        public override void OnScreenLoading()
        {
            timerToggle.onValueChanged.AddListener(TimerToggleValueChanged);
            movesToggle.onValueChanged.AddListener(MovesToggleValueChanged);
        }

        private void TimerToggleValueChanged(bool value)
        {
            OnTimerToggleValueChanged?.Invoke(value);
        }
        
        private void MovesToggleValueChanged(bool value)
        {
            OnMovesToggleValueChanged?.Invoke(value);
        }

        public void RetryButtonClick()
        {
            Navigation.Push<StartNewGamePopup>();
        }

        public void MainMenuButtonClick()
        {
            Navigation.Push<LeaveGameScreenPopup>();
        }

        public void CloseClick()
        {
            Navigation.Pop();
        }

        public override void OnScreenClose()
        {
            Closed?.Invoke(this);
        }
    }
}