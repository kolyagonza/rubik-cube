using Navigation;
using Zenject;

namespace RubikGame.Screens
{
    public class StartNewGamePopup : BaseScreen
    {
        private PlayerModel playerModel;
        
        [Inject]
        private void Init(PlayerModel playerModel)
        {
            this.playerModel = playerModel;
        }
        
        public void YesClicked()
        {
            var currentGameMode = playerModel.GameMode;
            playerModel.StartNewSave();
            playerModel.GameMode = currentGameMode;
            Navigation.PopToRoot();
            Navigation.Push<GameScreen>();
        }

        public void NoClicked()
        {
            Navigation.Pop();
        }
    }
}