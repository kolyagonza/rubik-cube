using System.Collections;
using System.Collections.Generic;
using Navigation;
using RubikGame;
using UnityEngine;
using Zenject;

public class LeaveGameScreenPopup : BaseScreen
{
    public void OnNoClick()
    {
        Navigation.Pop();
    }

    public void OnYesClick()
    {
        Navigation.PopToRoot();
        Navigation.Push<MainScreen>();
    }
}
