using System;
using System.Collections;
using System.Collections.Generic;
using Navigation;
using RubikGame;
using RubikGame.Core;
using UnityEngine;
using Zenject;

public class ChooseGameModeScreen : BaseScreen
{
    [SerializeField]
    private Transform buttonsContainerTransform;
    
    private PlayerModel playerModel;
    private GameModeButton.Factory buttonFactory;

    [Inject]
    private void Init(PlayerModel playerModel, GameModeButton.Factory buttonFactory)
    {
        this.playerModel = playerModel;
        this.buttonFactory = buttonFactory;
    }

    public override void OnScreenLoading()
    {
        foreach (var mode in Enum.GetValues(typeof(GameMode)))
        {
            var gameMode = (GameMode)mode;
            var button = buttonFactory.Create(gameMode);
            button.transform.SetParent(buttonsContainerTransform, false);
            button.OnButtonClicked += OnGameModeClicked;
            button.Destroyed -= OnGameModeClicked;
        }
    }

    public void OnBackgroundClick()
    {
        Navigation.Pop();
    }

    private void OnGameModeClicked(GameMode gameMode)
    {
        playerModel.StartNewSave();
        playerModel.GameMode = gameMode;
        Navigation.PopToRoot();
        Navigation.Push<GameScreen>();
    }
}
