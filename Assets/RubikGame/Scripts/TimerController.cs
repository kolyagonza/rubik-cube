using RubikGame;
using UnityEngine;
using Zenject;

public class TimerController : MonoBehaviour, ITickable
{
    private PlayerModel playerModel;
    private TickableManager tickableManager;
    private GameStateResolver gameStateResolver;
    private float currentTime;

    [Inject]
    private void Init(PlayerModel playerModel, TickableManager tickableManager, GameStateResolver gameStateResolver)
    {
        this.playerModel = playerModel;
        this.tickableManager = tickableManager;
        this.gameStateResolver = gameStateResolver;
        tickableManager.Add(this);
    }

    public void Tick()
    {
        if (gameStateResolver.CurrentState == GameStateResolver.GameState.Game)
        {
            if (currentTime < 1f)
            {
                currentTime += Time.deltaTime;
                return;
            }

            currentTime -= 1f;
            playerModel.IncrementTimer();
        }
    }

    private void OnDestroy()
    {
        tickableManager.Remove(this);
    }
}
