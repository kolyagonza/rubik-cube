﻿using RubikGame.Core;
using UnityEngine;

namespace RubikGame
{
    [CreateAssetMenu(fileName = "Settings", menuName = "Settings/CreateSettingsScriptableObject", order = 1)]
    public class SettingsContainer : ScriptableObject
    {
        public Cube.ConcreteFactory.Settings CubePrefab;
        public GameModeButton.Factory.Settings GameModeButtonSettings;
    }
}