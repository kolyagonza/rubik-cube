﻿using System;
using RubikGame.Core;
using TMPro;
using UnityEngine;
using UnityEngine.UI;
using Zenject;

namespace RubikGame
{
    [RequireComponent(typeof(Button))]
    public class GameModeButton : MonoBehaviour
    {
        [SerializeField] 
        private TextMeshProUGUI buttonText;

        private GameMode buttonMode;

        public event Action<GameMode> OnButtonClicked;
        public event Action<GameMode> Destroyed;
        
        [Inject]
        private void Init(GameMode mode)
        {
            buttonMode = mode;
            var modeName = mode.GetDescription();
            buttonText.text = modeName;
        }

        public void ButtonClick()
        {
            OnButtonClicked?.Invoke(buttonMode);
        }

        private void OnDestroy()
        {
            Destroyed?.Invoke(buttonMode);
        }

        public class Factory : PlaceholderFactory<GameMode, GameModeButton>
        {
            [Serializable]
            public class Settings
            {
                public GameModeButton ButtonPrefab;
            }
        }
    }
}