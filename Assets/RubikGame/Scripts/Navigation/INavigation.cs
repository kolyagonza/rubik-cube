﻿using System.Collections.Generic;

namespace Navigation
{
    public interface INavigation
    {
        BaseScreen Push<TScreen>() where TScreen : BaseScreen;
        void Pop();
        void PopToRoot();
        void EnableInput();
        void DisableInput();
    }
}