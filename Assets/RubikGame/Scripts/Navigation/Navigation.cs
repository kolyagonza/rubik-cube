﻿using System;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using UnityEngine.UI;
using Zenject;

namespace Navigation
{
    public class Navigation : MonoBehaviour, INavigation
    {
        [SerializeField] 
        private Transform rootObject;
        
        [SerializeField] 
        private GameObject inputBlocker;
        
        [SerializeField]
        private GraphicRaycaster graphicRaycaster;

        private ScreensContainer screensContainer;
        private DiContainer container;
        private readonly Stack<BaseScreen> screensStack = new Stack<BaseScreen>();


        [Inject]
        private void Init(ScreensContainer screensContainer, DiContainer container)
        {
            this.screensContainer = screensContainer;
            this.container = container;
        }

        public BaseScreen Push<TScreen>() where TScreen : BaseScreen
        {
            var targetScreen = screensContainer.Screens.FirstOrDefault(x => x is TScreen);
            if (targetScreen == null)
            {
                Debug.LogError("Trying to push invalid screen");
                return null;
            }

            var screen = Instantiate(targetScreen, rootObject);
            screen.Navigation = this;
            container.InjectGameObject(screen.gameObject);
            screen.OnScreenLoading();
            screensStack.Push(screen);
            return screen;
        }

        public void Pop()
        {
            var screen = screensStack.Pop();
            screen.OnScreenClose();
            Destroy(screen.gameObject);
            if (screensStack.Count > 0)
            {
                screensStack.Peek().Resume();
            }
        }

        public void PopToRoot()
        {
            while (screensStack.Count > 0)
            {
                Pop();
            }
        }

        public void DisableInput()
        {
            graphicRaycaster.enabled = false;
            if (inputBlocker != null)
            {
                inputBlocker.SetActive(true);
            }
        }

        public void EnableInput()
        {
            graphicRaycaster.enabled = true;
            if (inputBlocker != null)
            {
                inputBlocker.SetActive(false);
            }
        }
    }
}