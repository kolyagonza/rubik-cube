﻿using UnityEngine;

namespace Navigation
{
    public class BaseScreen : MonoBehaviour
    {
        public INavigation Navigation;
        
        public virtual void OnScreenLoading()
        {
        }

        public virtual void OnScreenClose()
        {
        }

        public virtual void Resume()
        {
        }
    }
}