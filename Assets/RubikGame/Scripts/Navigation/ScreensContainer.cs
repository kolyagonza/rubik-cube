﻿using System.Collections.Generic;
using UnityEngine;

namespace Navigation
{
    [CreateAssetMenu(fileName = "Screens", menuName = "Navigation/CreateScreensScriptableObject", order = 1)]
    public class ScreensContainer : ScriptableObject
    {
        public List<BaseScreen> Screens;
    }
}