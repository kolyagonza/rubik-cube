using System;

namespace RubikGame.Core
{
    public interface ICommand
    {
        public event Action<ICommand> CommandDone; 
        void Execute(CubeMover mover);
        void Undo(CubeMover mover);
        public bool Record { get; set; }
    }
}