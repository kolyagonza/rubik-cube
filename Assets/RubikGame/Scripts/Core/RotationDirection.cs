namespace RubikGame.Core
{
    public enum RotationDirection
    {
        None,
        HorizontalClockwise,
        HorizontalCounterClockwise,
        VerticalClockwise,
        VerticalCounterClockwise
    }
}