using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

namespace RubikGame.Core
{
    public class WinConditionChecker
    {
        public event Action GameWon;
        private PlayerModel playerModel;
        private CubeMover cubeMover;
        private GameStateResolver gameStateResolver;
        private CubeProcessor cubeProcessor;
        public WinConditionChecker(PlayerModel playerModel, CubeMover cubeMover, GameStateResolver gameStateResolver)
        {
            this.playerModel = playerModel;
            this.cubeMover = cubeMover;
            this.gameStateResolver = gameStateResolver;
            cubeMover.ActionDone += CheckIfGameWon;
        }

        private void CheckIfGameWon()
        {
            if (gameStateResolver.CurrentState == GameStateResolver.GameState.Game)
            {
                if (Enum.GetValues(typeof(PieceInfo.Faces)).Cast<PieceInfo.Faces>().Where(faceValue => faceValue != PieceInfo.Faces.None).Any(ContainsDifferentColorsOnFace))
                {
                    return;
                }

                GameWon?.Invoke();
            }
        }

        private bool ContainsDifferentColorsOnFace(PieceInfo.Faces face)
        {
            var firstColor = Vector3.zero;
            foreach (var bottomCube in playerModel.CubePieces.Values.Where(x => x.HasFace(face)))
            {
                if (firstColor == Vector3.zero)
                {
                    firstColor = bottomCube.GetColor(face);
                    continue;
                }

                var cubeColor = bottomCube.GetColor(face);
                if (cubeColor != firstColor)
                {
                    return true;
                }
            }

            return false;
        }
    }
}