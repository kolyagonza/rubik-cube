using System;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using Zenject;

namespace RubikGame.Core
{
    public class CubeGenerator
    {
        private Transform targetContainer;
        private Cube.Factory cubeFactory;
        private PlayerModel playerModel;

        public Transform CubesContainer => targetContainer;

        public CubeGenerator(Transform targetContainer, Cube.Factory cubeFactory, PlayerModel playerModel)
        {
            this.targetContainer = targetContainer;
            this.cubeFactory = cubeFactory;
            this.playerModel = playerModel;
        }

        public void GenerateCube(GameMode gameMode, List<PieceInfo> oldData = null)
        {
            targetContainer.rotation = Quaternion.identity;
            var cubeLength = (int)gameMode;
            var delta = (cubeLength - 1) / 2f;
            var startPosition = targetContainer.position - new Vector3(delta, delta, delta);
            for (var height = 0; height < cubeLength; height++)
            {
                for (var width = 0; width < cubeLength; width++)
                {
                    for (var length = 0; length < cubeLength; length++)
                    {
                        var position = new Vector3(startPosition.x + width, startPosition.y + height,
                            startPosition.z + length);
                        var index = height * cubeLength * cubeLength + width * cubeLength + length;
                        PieceInfo pieceInfo;
                        var flag = GetFacesFromIndex(index, cubeLength);
                        if (oldData == null)
                        {
                            pieceInfo = new PieceInfo
                            {
                                //StartIndex = index,
                                Index = index,
                                FacesFlag = flag,
                                FacesColor = GenerateColorFaces(flag)
                            };
                        }
                        else
                        {
                            pieceInfo = oldData.First(x => x.Index == index);
                            pieceInfo.FacesFlag = flag;
                        }

                        playerModel.CubePieces.Add(index, cubeFactory.Create(targetContainer, position, pieceInfo));
                    }
                }
            }
        }

        private PieceInfo.Faces GetFacesFromIndex(int index, int length)
        {
            var faces = PieceInfo.Faces.None;
            var sliceLength = length * length;
            var sliceIndex = Mathf.FloorToInt(index / (float)sliceLength);
            var positionInSlice = index % sliceLength;

            if (sliceIndex == 0)
            {
                faces |= PieceInfo.Faces.Bot;
            }

            if (sliceIndex == length - 1)
            {
                faces |= PieceInfo.Faces.Top;
            }

            if (positionInSlice % length == 0)
            {
                faces |= PieceInfo.Faces.Back;
            }

            if ((positionInSlice + 1) % length == 0)
            {
                faces |= PieceInfo.Faces.Forward;
            }

            if (positionInSlice < length)
            {
                faces |= PieceInfo.Faces.Left;
            }

            if (positionInSlice >= length * length - length)
            {
                faces |= PieceInfo.Faces.Right;
            }

            return faces;
        }

        private Dictionary<PieceInfo.Faces, Vector3> GenerateColorFaces(PieceInfo.Faces flag)
        {
            var dict = new Dictionary<PieceInfo.Faces, Vector3>();
            foreach (var face in Enum.GetValues(typeof(PieceInfo.Faces)))
            {
                var value = (PieceInfo.Faces)face;
                if (value == PieceInfo.Faces.None)
                {
                    continue;
                }
                
                if (flag.HasFlag(value))
                {
                    var color = playerModel.FacesColorInfo[value];
                    dict[value] = new Vector3(color.r, color.g, color.b);
                }

            }

            return dict;
        }

        public void DestroyCurrentCube()
        {
            foreach (var cube in playerModel.CubePieces)
            {
                cube.Value.DestroyCube();
            }
            
            playerModel.CubePieces.Clear();
        }
    }
}
