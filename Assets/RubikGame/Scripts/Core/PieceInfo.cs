﻿using System;
using System.Collections.Generic;
using System.Runtime.Serialization;
using Newtonsoft.Json;
using UnityEngine;

namespace RubikGame.Core
{
    [Serializable]
    [DataContract]
    public struct PieceInfo
    {
        [Flags]
        public enum Faces
        {
            None = 0,
            Top = 1,
            Bot = 2,
            Left = 4,
            Right = 8,
            Forward = 16,
            Back = 32
        }

        //[DataMember(Name = "StartIndex")]
        //public int StartIndex;
        [DataMember(Name = "CurrentIndex")]
        public int Index;
        [DataMember(Name = "ColorsContainer")]
        public Dictionary<Faces, Vector3> FacesColor;
        [JsonIgnore]
        public Faces FacesFlag;

    }
}