using System.Collections.Generic;
using System.Linq;
using UnityEngine;

namespace RubikGame.Core
{
    public class VerticalClockwiseRotateCommand : BaseRotateCommand
    {
        protected override Vector3 Axis => Vector3.left;
        protected override int TargetRotation => 90;
        private readonly Cube selectedCube;
        protected override RotationDirection rotationDirection => RotationDirection.VerticalClockwise;



        public VerticalClockwiseRotateCommand(CubeProcessor cubeProcessor, Transform cubesContainer, int selectedCubeIndex)
            : base(cubeProcessor, cubesContainer, selectedCubeIndex)
        {
        }

        public VerticalClockwiseRotateCommand(List<Cube> cubes, Vector3 rotationMiddlePoint,
            CubeProcessor cubeProcessor, Transform cubesContainer) : base (cubes, rotationMiddlePoint,cubeProcessor, cubesContainer)
        {
            
        }
        
        public override void Execute(CubeMover mover)
        {
            if (selectedCubeIndex != -1)
            {
                cubesToRotate = cubeProcessor.GetVerticalSide(selectedCubeIndex, out middlePoint).ToList();
            }

            mover.StartCoroutine(MoveRoutine());
            cubeProcessor.ShiftClockwise(cubesToRotate, rotationDirection);
        }

        public override void Undo(CubeMover mover)
        {
            var cubes = cubeProcessor.GetVerticalSide(selectedCubeIndex, out var rotationMiddlePoint).ToList();
            var command = new VerticalCounterClockwiseRotateCommand(cubes, rotationMiddlePoint, cubeProcessor, cubesContainer)                {
                Record = false
            };
            mover.AddCommand(command);
        }
    }
}