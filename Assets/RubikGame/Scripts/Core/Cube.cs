﻿using System;
using System.Collections.Generic;
using Newtonsoft.Json;
using UnityEngine;
using Zenject;

namespace RubikGame.Core
{
    [RequireComponent(typeof(MeshFilter))]
    [RequireComponent(typeof(MeshRenderer))]
    public class Cube : MonoBehaviour
    {
        [SerializeField] 
        private MeshRenderer topFace;
        [SerializeField] 
        private MeshRenderer bottomFace;
        [SerializeField] 
        private MeshRenderer leftFace;
        [SerializeField] 
        private MeshRenderer rightFace;
        [SerializeField] 
        private MeshRenderer forwardFace;
        [SerializeField] 
        private MeshRenderer backFace;
        [SerializeField]
        private Color selectedColor;

        private MeshRenderer originMesh;
        private Color startColor;
        private static readonly int ColorProperty = Shader.PropertyToID("_Color");
        private PieceInfo pieceInfo;

        public int Index
        {
            get => pieceInfo.Index;
            set => pieceInfo.Index = value;
        }

        [Inject]
        private void Init(PieceInfo pieceInfo, PlayerModel playerModel)
        {
            this.pieceInfo = pieceInfo;
            originMesh = GetComponent<MeshRenderer>();
            startColor = originMesh.material.color;
            DeactivateAll();
            
            if (pieceInfo.FacesFlag.HasFlag(PieceInfo.Faces.Top))
            {
                SetColor(topFace, PieceInfo.Faces.Top);
            }

            if (pieceInfo.FacesFlag.HasFlag(PieceInfo.Faces.Bot))
            {
                SetColor(bottomFace, PieceInfo.Faces.Bot);
            }

            if (pieceInfo.FacesFlag.HasFlag(PieceInfo.Faces.Left))
            {
                SetColor(leftFace, PieceInfo.Faces.Left);
            }

            if (pieceInfo.FacesFlag.HasFlag(PieceInfo.Faces.Right))
            {
                SetColor(rightFace, PieceInfo.Faces.Right);
            }

            if (pieceInfo.FacesFlag.HasFlag(PieceInfo.Faces.Forward))
            {
                SetColor(forwardFace,  PieceInfo.Faces.Forward);
            }

            if (pieceInfo.FacesFlag.HasFlag(PieceInfo.Faces.Back))
            {
                SetColor(backFace, PieceInfo.Faces.Back);
            }
        }

        public void ShiftCurrentFaces(RotationDirection direction)
        {
            var currentFacesFlag = pieceInfo.FacesFlag;
            var targetFlag = PieceInfo.Faces.None;
            var targetDictionary = new Dictionary<PieceInfo.Faces, Vector3>();
            switch (direction)
            {
                case RotationDirection.HorizontalClockwise:
                    if (currentFacesFlag.HasFlag(PieceInfo.Faces.Top))
                    {
                        targetFlag |= PieceInfo.Faces.Top;
                        targetDictionary.Add(PieceInfo.Faces.Top, pieceInfo.FacesColor[PieceInfo.Faces.Top]);
                    }
                    if (currentFacesFlag.HasFlag(PieceInfo.Faces.Bot))
                    {
                        targetFlag |= PieceInfo.Faces.Bot;
                        targetDictionary.Add(PieceInfo.Faces.Bot, pieceInfo.FacesColor[PieceInfo.Faces.Bot]);
                    }
                    if (currentFacesFlag.HasFlag(PieceInfo.Faces.Back))
                    {
                        FillTargetValues(ref targetFlag, targetDictionary,PieceInfo.Faces.Back, PieceInfo.Faces.Left);
                    }
                    
                    if (currentFacesFlag.HasFlag(PieceInfo.Faces.Forward))
                    {
                        FillTargetValues(ref targetFlag, targetDictionary,PieceInfo.Faces.Forward, PieceInfo.Faces.Right);
                    }
                    
                    if (currentFacesFlag.HasFlag(PieceInfo.Faces.Left))
                    {
                        FillTargetValues(ref targetFlag, targetDictionary, PieceInfo.Faces.Left, PieceInfo.Faces.Forward);
                    }
                    
                    if (currentFacesFlag.HasFlag(PieceInfo.Faces.Right))
                    {
                        FillTargetValues(ref targetFlag, targetDictionary,PieceInfo.Faces.Right, PieceInfo.Faces.Back);
                    }
                    break;
                case RotationDirection.HorizontalCounterClockwise:
                    if (currentFacesFlag.HasFlag(PieceInfo.Faces.Top))
                    {
                        targetFlag |= PieceInfo.Faces.Top;
                        targetDictionary.Add(PieceInfo.Faces.Top, pieceInfo.FacesColor[PieceInfo.Faces.Top]);
                    }
                    if (currentFacesFlag.HasFlag(PieceInfo.Faces.Bot))
                    {
                        targetFlag |= PieceInfo.Faces.Bot;
                        targetDictionary.Add(PieceInfo.Faces.Bot, pieceInfo.FacesColor[PieceInfo.Faces.Bot]);
                    }
                    if (currentFacesFlag.HasFlag(PieceInfo.Faces.Forward))
                    {
                        FillTargetValues(ref targetFlag, targetDictionary,PieceInfo.Faces.Forward, PieceInfo.Faces.Left);
                    }
                    
                    if (currentFacesFlag.HasFlag(PieceInfo.Faces.Back))
                    {
                        FillTargetValues(ref targetFlag, targetDictionary,PieceInfo.Faces.Back, PieceInfo.Faces.Right);
                    }
                    
                    if (currentFacesFlag.HasFlag(PieceInfo.Faces.Right))
                    {
                        FillTargetValues(ref targetFlag, targetDictionary,PieceInfo.Faces.Right, PieceInfo.Faces.Forward);
                    }
                    
                    if (currentFacesFlag.HasFlag(PieceInfo.Faces.Left))
                    {
                        FillTargetValues(ref targetFlag, targetDictionary,PieceInfo.Faces.Left, PieceInfo.Faces.Back);
                    }
                    break;
                case RotationDirection.VerticalClockwise:
                    if (currentFacesFlag.HasFlag(PieceInfo.Faces.Left))
                    {
                        targetFlag |= PieceInfo.Faces.Left;
                        targetDictionary.Add(PieceInfo.Faces.Left, pieceInfo.FacesColor[PieceInfo.Faces.Left]);
                    }
                    if (currentFacesFlag.HasFlag(PieceInfo.Faces.Right))
                    {
                        targetFlag |= PieceInfo.Faces.Right;
                        targetDictionary.Add(PieceInfo.Faces.Right, pieceInfo.FacesColor[PieceInfo.Faces.Right]);
                    }
                    if (currentFacesFlag.HasFlag(PieceInfo.Faces.Forward))
                    {
                        FillTargetValues(ref targetFlag, targetDictionary,PieceInfo.Faces.Forward, PieceInfo.Faces.Top);
                    }

                    if (currentFacesFlag.HasFlag(PieceInfo.Faces.Back))
                    {
                        FillTargetValues(ref targetFlag, targetDictionary,PieceInfo.Faces.Back, PieceInfo.Faces.Bot);
                    }
                    
                    if (currentFacesFlag.HasFlag(PieceInfo.Faces.Bot))
                    {
                        FillTargetValues(ref targetFlag, targetDictionary,PieceInfo.Faces.Bot, PieceInfo.Faces.Forward);
                    }
                    
                    if (currentFacesFlag.HasFlag(PieceInfo.Faces.Top))
                    {
                        FillTargetValues(ref targetFlag, targetDictionary,PieceInfo.Faces.Top, PieceInfo.Faces.Back);
                    }
                    break;
                case RotationDirection.VerticalCounterClockwise:
                    if (currentFacesFlag.HasFlag(PieceInfo.Faces.Left))
                    {
                        targetFlag |= PieceInfo.Faces.Left;
                        targetDictionary.Add(PieceInfo.Faces.Left, pieceInfo.FacesColor[PieceInfo.Faces.Left]);
                    }
                    if (currentFacesFlag.HasFlag(PieceInfo.Faces.Right))
                    {
                        targetFlag |= PieceInfo.Faces.Right;
                        targetDictionary.Add(PieceInfo.Faces.Right, pieceInfo.FacesColor[PieceInfo.Faces.Right]);
                    }
                    if (currentFacesFlag.HasFlag(PieceInfo.Faces.Back))
                    {
                        FillTargetValues(ref targetFlag, targetDictionary,PieceInfo.Faces.Back, PieceInfo.Faces.Top);
                    }

                    if (currentFacesFlag.HasFlag(PieceInfo.Faces.Forward))
                    {
                        FillTargetValues(ref targetFlag, targetDictionary,PieceInfo.Faces.Forward, PieceInfo.Faces.Bot);
                    }

                    if (currentFacesFlag.HasFlag(PieceInfo.Faces.Top))
                    {
                        FillTargetValues(ref targetFlag, targetDictionary,PieceInfo.Faces.Top, PieceInfo.Faces.Forward);
                    }
                    
                    if (currentFacesFlag.HasFlag(PieceInfo.Faces.Bot))
                    {
                        FillTargetValues(ref targetFlag, targetDictionary,PieceInfo.Faces.Bot, PieceInfo.Faces.Back);
                    }
                    break;
            }

            pieceInfo.FacesFlag = targetFlag;
            pieceInfo.FacesColor = targetDictionary;
        }

        public bool HasFace(PieceInfo.Faces face)
        {
            return pieceInfo.FacesFlag.HasFlag(face);
        }

        public Vector3 GetColor(PieceInfo.Faces face)
        {
            return pieceInfo.FacesColor[face];
        }

        private void FillTargetValues(ref PieceInfo.Faces targetFace, Dictionary<PieceInfo.Faces, Vector3> colors, PieceInfo.Faces oldFace, PieceInfo.Faces newFace)
        {
            targetFace |= newFace;
            colors.Add(newFace, pieceInfo.FacesColor[oldFace]);
        }

        private void DeactivateAll()
        {
            topFace.gameObject.SetActive(false);
            bottomFace.gameObject.SetActive(false);
            leftFace.gameObject.SetActive(false);
            rightFace.gameObject.SetActive(false);
            forwardFace.gameObject.SetActive(false);
            backFace.gameObject.SetActive(false);
        }

        private void SetColor(Renderer meshRenderer, PieceInfo.Faces face)
        {
            var colorValues = pieceInfo.FacesColor[face];
            var color = new Color(colorValues.x, colorValues.y, colorValues.z);
            meshRenderer.gameObject.SetActive(true);
            meshRenderer.material.SetColor(ColorProperty, color);
        }

        public void OnPointerEnter()
        {
            originMesh.material.color = selectedColor;
        }

        public void OnPointerExit()
        {
            originMesh.material.color = startColor;
        }

        public void DestroyCube()
        {
            Destroy(gameObject);
        }

        public string Render()
        {
            var value = JsonConvert.SerializeObject(pieceInfo);
            return value;
        }
        
        public class ConcreteFactory : IFactory<Transform, Vector3, PieceInfo, Cube>
        {
            private Settings settings;
            private DiContainer container;
            
            public ConcreteFactory(Settings settings, DiContainer container)
            {
                this.settings = settings;
                this.container = container;
            }

            public Cube Create(Transform parent, Vector3 position, PieceInfo pieceInfo)
            {
                var args = new object[] { pieceInfo };
                var createdObject = container.InstantiatePrefabForComponent<Cube>(settings.CubePrefab, args);
                createdObject.transform.position = position;
                createdObject.transform.SetParent(parent, true);
                
                return createdObject;
            }
            
            [Serializable]
            public class Settings
            {
                public Cube CubePrefab;
            }
        }

        public class Factory : PlaceholderFactory<Transform, Vector3, PieceInfo, Cube>
        {
        }
    }
}