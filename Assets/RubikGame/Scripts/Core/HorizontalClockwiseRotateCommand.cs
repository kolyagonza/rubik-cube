using System.Collections.Generic;
using System.Linq;
using UnityEngine;

namespace RubikGame.Core
{
    public class HorizontalClockwiseRotateCommand : BaseRotateCommand
    {
        protected override Vector3 Axis => Vector3.up;
        protected override int TargetRotation => 90;
        protected override RotationDirection rotationDirection => RotationDirection.HorizontalClockwise;

        public HorizontalClockwiseRotateCommand(CubeProcessor cubeProcessor, Transform cubesContainer, int selectedCubeIndex) 
            : base(cubeProcessor, cubesContainer, selectedCubeIndex)
        {
        }
        
        public HorizontalClockwiseRotateCommand(List<Cube> cubes, Vector3 rotationMiddlePoint,
            CubeProcessor cubeProcessor, Transform cubesContainer) : base (cubes, rotationMiddlePoint,cubeProcessor, cubesContainer)
        {
            
        }
        
        public override void Execute(CubeMover mover)
        {
            if (selectedCubeIndex != -1)
            {
                cubesToRotate = cubeProcessor.GetHorizontalSide(selectedCubeIndex, out middlePoint).ToList();
            }

            cubeProcessor.ShiftClockwise(cubesToRotate, rotationDirection);
            mover.StartCoroutine(MoveRoutine());
        }

        public override void Undo(CubeMover mover)
        {
            var cubes = cubeProcessor.GetHorizontalSide(selectedCubeIndex, out middlePoint).ToList();
            var command = new HorizontalCounterClockwiseRotateCommand(cubes, middlePoint, cubeProcessor, cubesContainer)                {
                Record = false
            };
            mover.AddCommand(command);
        }
    }
}