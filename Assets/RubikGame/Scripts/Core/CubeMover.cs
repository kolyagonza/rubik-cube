using System;
using System.Collections.Generic;
using System.Linq;
using Navigation;
using UnityEngine;
using Zenject;
using Random = UnityEngine.Random;

namespace RubikGame.Core
{
    public class CubeMover : MonoBehaviour
    {
        public event Action<bool> UndoStateChanged;
        public event Action ActionDone;
        private List<ICommand> executedCommands = new List<ICommand>();
        private INavigation navigation;
        private bool canUndo;
        private Queue<ICommand> commandsInQueue = new Queue<ICommand>();
        private ICommand currentCommand;

        public bool IsBusy => currentCommand != null;

        private bool CanUndo
        {
            set
            {
                if (canUndo != value)
                {
                    canUndo = value;
                    UndoStateChanged?.Invoke(canUndo);
                }
            }
        }

        [Inject]
        private void Init(INavigation navigation)
        {
            this.navigation = navigation;
        }

        public void AddCommand(ICommand command)
        {
            if (currentCommand == null)
            {
                ExecuteCommand(command);
            }
            else
            {
                commandsInQueue.Enqueue(command);
            }
        }

        public void UndoCommand()
        {
            if (executedCommands.Count > 0)
            {
                var command = executedCommands.Last();
                command.Undo(this);
                executedCommands.RemoveAt(executedCommands.Count - 1);
                FireUndoStateChangeEventIfNeeded();
            }
        }

        private void FireUndoStateChangeEventIfNeeded()
        {
            CanUndo = executedCommands.Count > 0;
        }

        private void ExecuteCommand(ICommand command)
        {
            currentCommand = command;
            navigation.DisableInput();
            command.Execute(this);
            command.CommandDone += OnCommandDone;
            if (command.Record)
            {
                executedCommands.Add(command);
            }

            FireUndoStateChangeEventIfNeeded();
            ActionDone?.Invoke();
        }

        private void OnCommandDone(ICommand command)
        {
            command.CommandDone -= OnCommandDone;
            if (commandsInQueue.Count == 0)
            {
                currentCommand = null;
                navigation.EnableInput();
                return;
            }
            
            ExecuteCommand(commandsInQueue.Dequeue());
        }
    }
}