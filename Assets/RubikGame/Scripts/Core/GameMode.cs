﻿using System.ComponentModel;

namespace RubikGame.Core
{
    public enum GameMode
    {
        [Description("2x2")]
        TwoOnTwo = 2,
        [Description("3x3")]
        ThreeOnThree = 3,
        [Description("4x4")]
        FourOnFour = 4,
        [Description("5x5")]
        FiveOnFive = 5,
        [Description("6x6")]
        SixOnSix = 6
    }
}