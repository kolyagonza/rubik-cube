using System.Collections.Generic;
using System.Linq;
using UnityEngine;

namespace RubikGame.Core
{
    public class HorizontalCounterClockwiseRotateCommand : BaseRotateCommand
    {
        protected override Vector3 Axis => Vector3.up;
        protected override int TargetRotation => -90;
        protected override RotationDirection rotationDirection => RotationDirection.HorizontalCounterClockwise;


        public HorizontalCounterClockwiseRotateCommand(CubeProcessor cubeProcessor, Transform cubesContainer, int selectedCubeIndex) 
            : base(cubeProcessor, cubesContainer, selectedCubeIndex)
        {
        }
        
        public HorizontalCounterClockwiseRotateCommand(List<Cube> cubes, Vector3 rotationMiddlePoint,
            CubeProcessor cubeProcessor, Transform cubesContainer) : base (cubes, rotationMiddlePoint,cubeProcessor, cubesContainer)
        {
            
        }
        
        public override void Execute(CubeMover mover)
        {
            if (selectedCubeIndex != -1)
            {
                cubesToRotate = cubeProcessor.GetHorizontalSide(selectedCubeIndex, out middlePoint).ToList();
            }
            
            mover.StartCoroutine(MoveRoutine());
            cubeProcessor.ShiftCounterClockwise(cubesToRotate, rotationDirection);
        }

        public override void Undo(CubeMover mover)
        {
            var cubes = cubeProcessor.GetHorizontalSide(selectedCubeIndex, out middlePoint).ToList();
            var command = new HorizontalClockwiseRotateCommand(cubes, middlePoint, cubeProcessor, cubesContainer)                {
                Record = false
            };
            mover.AddCommand(command);
        }
    }
}