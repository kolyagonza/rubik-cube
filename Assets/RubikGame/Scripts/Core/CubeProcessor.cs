﻿using System.Collections.Generic;
using System.Linq;
using System.Text;
using DG.Tweening;
using UnityEngine;
using Zenject;

namespace RubikGame.Core
{
    public class CubeProcessor
    {
        private PlayerModel playerModel;
        
        public CubeProcessor(PlayerModel playerModel)
        {
            this.playerModel = playerModel;
        }

        public IEnumerable<Cube> GetMovableCubes(int index)
        {
            var movableCubes = new HashSet<Cube>();
            var step = (int)playerModel.GameMode;
            var sideIndex = Mathf.FloorToInt(index / ((float)step * step));
            var positionInLayer = index % (step * step);
            var column = Mathf.FloorToInt(positionInLayer / (float)step);
            var row = positionInLayer % step;
            
            AddColumnLength(movableCubes, column, step, sideIndex);
            AddRow(movableCubes, row, step, sideIndex);
            AddColumnHeight(movableCubes, column, row, step);

            return movableCubes;
        }

        public IEnumerable<Cube> GetHorizontalSide(int index, out Vector3 middlePoint)
        {
            var movableCubes = new HashSet<Cube>();
            var step = (int)playerModel.GameMode;
            var sideIndex = Mathf.FloorToInt(index / ((float)step * step));
            for (var i = 0; i < step; i++)
            {
                AddColumnLength(movableCubes, i, step, sideIndex);
            }

            middlePoint = movableCubes.Aggregate(Vector3.zero, (current, cube) => current + cube.transform.position);
            middlePoint /= movableCubes.Count;

            return movableCubes;
        }
        
        public IEnumerable<Cube> GetVerticalSide(int index, out Vector3 middlePoint)
        {
            var movableCubes = new HashSet<Cube>();
            var step = (int)playerModel.GameMode;
            var positionInLayer = index % (step * step);
            var column = Mathf.FloorToInt(positionInLayer / (float)step);    
            for (var i = step - 1; i >= 0 ; i--)
            {
                AddColumnHeight(movableCubes, column, i, step);
            }

            middlePoint = movableCubes.Aggregate(Vector3.zero, (current, cube) => current + cube.transform.position);
            middlePoint /= movableCubes.Count;

            return movableCubes;
        }

        private void AddColumnLength(HashSet<Cube> cubes, int column, int size, int layer)
        {
            for (var i = 0; i < size; i++)
            {
                cubes.Add(playerModel.CubePieces[i + size * column + layer * size * size]);
            }
        }
        
        private void AddRow(HashSet<Cube> cubes, int row, int size, int layer)
        {
            for (var i = 0; i < size; i++)
            {
                cubes.Add(playerModel.CubePieces[i * size + row + layer * size * size]);
            }
        }

        private void AddColumnHeight(HashSet<Cube> cubes, int column, int row, int size)
        {
            for (var i = 0; i < size; i++)
            {
                cubes.Add(playerModel.CubePieces[column * size + row + i * size * size]);
            }
        }

        public void ShiftClockwise(List<Cube> cubesToShift, RotationDirection direction)
        {
            var matrix = GenerateMatrix(cubesToShift);
            var rotatedMatrix = RotateMatrixClockwise(matrix);
            ChangeValues(matrix, rotatedMatrix, direction);
        }
        
        public void ShiftCounterClockwise(List<Cube> cubesToShift, RotationDirection direction)
        {
            var matrix = GenerateMatrix(cubesToShift);
            var rotatedMatrix = RotateMatrixCounterClockwise(matrix);
            ChangeValues(matrix, rotatedMatrix, direction);
        }

        private int[,] GenerateMatrix(List<Cube> cubesToShift)
        {
            var step = (int)playerModel.GameMode;
            var matrix = new int[step, step];
            for (var row = 0; row < step; row++)
            {
                for (var column = 0; column < step; column++)
                {
                    matrix[step - 1 - column, row] = cubesToShift[column + row * step].Index;
                }
            }

            return matrix;
        }

        private void ChangeValues(int[,] originalMatrix, int[,] rotatedMatrix, RotationDirection direction)
        {
            var copiedDict = new Dictionary<int, Cube>(playerModel.CubePieces);
            for (var col = 0; col < rotatedMatrix.GetLength(0); col++)
            {
                for (var row = rotatedMatrix.GetLength(1) - 1; row >= 0; row--)
                {
                    var firstIndex = originalMatrix[row, col];
                    var secondIndex = rotatedMatrix[row, col];
                    playerModel.CubePieces[firstIndex] = copiedDict[secondIndex];
                    playerModel.CubePieces[firstIndex].Index = firstIndex;
                    playerModel.CubePieces[firstIndex].ShiftCurrentFaces(direction);
                }
            }

            playerModel.Save();
        }
        
        private static int[,] RotateMatrixCounterClockwise(int[,] oldMatrix)
        {
            var newMatrix = new int[oldMatrix.GetLength(1), oldMatrix.GetLength(0)];
            var newRow = 0;
            for (var oldColumn = oldMatrix.GetLength(1) - 1; oldColumn >= 0; oldColumn--)
            {
                var newColumn = 0;
                for (var oldRow = 0; oldRow < oldMatrix.GetLength(0); oldRow++)
                {
                    newMatrix[newRow, newColumn] = oldMatrix[oldRow, oldColumn];
                    newColumn++;
                }
                newRow++;
            }
            return newMatrix;
        }
        
        private static int[,] RotateMatrixClockwise(int[,] oldMatrix)
        {
            var height = oldMatrix.GetLength(1);
            var width = oldMatrix.GetLength(0);
            var newMatrix = new int[height, width];
            for (var i = 0; i < height; i++)
            {
                for (var j = 0; j < width; j++)
                {
                    newMatrix[j, width - 1 - i] = oldMatrix[i, j]; 
                }
            }

            return newMatrix;
        }
    }
}