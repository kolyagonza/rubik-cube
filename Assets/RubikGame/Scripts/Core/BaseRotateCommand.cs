using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace RubikGame.Core
{
    public abstract class BaseRotateCommand : ICommand
    {
        public event Action<ICommand> CommandDone;
        protected virtual Vector3 Axis => Vector3.zero;
        protected virtual int TargetRotation => 0;
        protected virtual RotationDirection rotationDirection => RotationDirection.None;
        protected List<Cube> cubesToRotate;
        protected Vector3 middlePoint;
        protected readonly CubeProcessor cubeProcessor;
        protected readonly Transform cubesContainer;
        protected readonly int selectedCubeIndex = -1;

        protected BaseRotateCommand(CubeProcessor cubeProcessor, Transform cubesContainer, int selectedCubeIndex)
        {
            this.cubeProcessor = cubeProcessor;
            this.cubesContainer = cubesContainer;
            this.selectedCubeIndex = selectedCubeIndex;
        }
        
        protected BaseRotateCommand(List<Cube> cubes, Vector3 rotationMiddlePoint, CubeProcessor cubeProcessor, Transform cubesContainer)
        {
            cubesToRotate = cubes;
            middlePoint = rotationMiddlePoint;
            this.cubeProcessor = cubeProcessor;
            this.cubesContainer = cubesContainer;
        }

        public abstract void Execute(CubeMover mover);

        public abstract void Undo(CubeMover mover);
        public bool Record { get; set; } = true;

        protected IEnumerator MoveRoutine()
        {
            var rotationDone = 0;
            var delta = TargetRotation / 30;
            var axis = Axis == Vector3.up ? cubesContainer.up : -cubesContainer.right;
            while (rotationDone != TargetRotation)
            {
                foreach (var cube in cubesToRotate)
                {
                    cube.transform.RotateAround(middlePoint, axis , delta);
                }
                rotationDone += delta;
                yield return null;
            }

            yield return null;
            CommandDone?.Invoke(this);
        }
    }
}