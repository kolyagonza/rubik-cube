using System.Collections.Generic;
using System.Linq;
using UnityEngine;

namespace RubikGame.Core
{
    public class VerticalCounterClockwiseRotateCommand : BaseRotateCommand
    {
        protected override Vector3 Axis => Vector3.left;
        protected override int TargetRotation => -90;
        protected override RotationDirection rotationDirection => RotationDirection.VerticalCounterClockwise;


        public VerticalCounterClockwiseRotateCommand(CubeProcessor cubeProcessor, Transform cubesContainer, int selectedCubeIndex) :
            base(cubeProcessor, cubesContainer, selectedCubeIndex)
        {
        }
        
        public VerticalCounterClockwiseRotateCommand(List<Cube> cubes, Vector3 rotationMiddlePoint,
            CubeProcessor cubeProcessor, Transform cubesContainer) : base (cubes, rotationMiddlePoint,cubeProcessor, cubesContainer)
        {
            
        }

        public override void Execute(CubeMover mover)
        {
            if (selectedCubeIndex != -1)
            {
                cubesToRotate = cubeProcessor.GetVerticalSide(selectedCubeIndex, out middlePoint).ToList();
            }

            mover.StartCoroutine(MoveRoutine());
            cubeProcessor.ShiftCounterClockwise(cubesToRotate, RotationDirection.VerticalCounterClockwise);
        }

        public override void Undo(CubeMover mover)
        {
            var cubes = cubeProcessor.GetVerticalSide(selectedCubeIndex, out var rotationMiddlePoint).ToList();
            var command = new VerticalClockwiseRotateCommand(cubes, rotationMiddlePoint, cubeProcessor, cubesContainer)
                {
                    Record = false
                };
            mover.AddCommand(command);
        }
    }
}