﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using Newtonsoft.Json;
using RubikGame.Core;
using UnityEngine;
using Zenject;

namespace RubikGame
{
    public class PlayerModel
    {
        public event Action<PlayerSave> PlayerSaveChanged;
        public Dictionary<PieceInfo.Faces, Color> FacesColorInfo;

        public Dictionary<int, Cube> CubePieces = new Dictionary<int, Cube>();

        public GameMode GameMode
        {
            get => playerSave.GameMode;
            set => playerSave.GameMode = value;
        }
        public bool ContinueFlag;
        public List<PieceInfo> ContinueData = new List<PieceInfo>();

        public bool ShowMoves
        {
            get => playerSave.ShowMoves;
            set
            {
                playerSave.ShowMoves = value;
                Save();
            }
        }
        
        public bool ShowTimer
        {
            get => playerSave.ShowTimer;
            set
            {
                playerSave.ShowTimer = value;
                Save();
            }
        }

        private PlayerSave playerSave;
        private const string PlayerSavePrefsString = "player_save";
        private CubeMover cubeMover;
        private GameStateResolver gameStateResolver;

        public PlayerModel(CubeMover cubeMover, GameStateResolver gameStateResolver)
        {
            this.cubeMover = cubeMover;
            this.gameStateResolver = gameStateResolver;
            cubeMover.ActionDone += CubeMoverOnActionDone;
        }

        private void CubeMoverOnActionDone()
        {
            if (gameStateResolver.CurrentState == GameStateResolver.GameState.Game)
            {
                playerSave.MovesCount++;
                Save();
            }
        }

        public void IncrementTimer()
        {
            playerSave.TimeSpend++;
            Save();
        }

        public void Load()
        {
            var lastSave = LoadLastPlayerSave();
            if (lastSave == null)
            {
                CreateInitialData();
            }
            else
            {
                playerSave = lastSave;
            }
        }

        public void StartNewSave()
        {
            if (HasSaveFile())
            {
                PlayerPrefs.DeleteKey(PlayerSavePrefsString);
                CreateInitialData();
            }

            ContinueFlag = false;
            ContinueData.Clear();
            SetCurrentRandomizedColorPalette(Utils.GetRandomColors(System.Enum.GetValues(typeof(PieceInfo.Faces)).Length - 1));
        }

        public void ContinueGame()
        {
            ContinueFlag = true;
            ContinueData.Clear();
            foreach (var data in playerSave.CubesData)
            {
                ContinueData.Add(JsonConvert.DeserializeObject<PieceInfo>(data));
            }
        }

        public void Save()
        {
            var cubesData = CubePieces.Values.Select(cube => cube.Render()).ToList();
            playerSave.CubesData = cubesData;
            playerSave.GameMode = GameMode;
            PlayerPrefs.SetString(PlayerSavePrefsString, JsonConvert.SerializeObject(playerSave));
            PlayerPrefs.Save();
            PlayerSaveChanged?.Invoke(playerSave);
        }

        public void GetCurrentTimeAndMovesValues(out int time, out int moves)
        {
            time = playerSave.TimeSpend;
            moves = playerSave.MovesCount;
        }

        private void CreateInitialData()
        {
            playerSave = new PlayerSave()
            {
                CubesData = new List<string>(),
                GameMode = GameMode.TwoOnTwo,
                MovesCount = 0,
                TimeSpend = 0,
                ShowMoves = true,
                ShowTimer = true
            };
        }

        private void SetCurrentRandomizedColorPalette(IReadOnlyList<Color> colors)
        {
            FacesColorInfo = new Dictionary<PieceInfo.Faces, Color>();
            var i = 0;
            foreach (var faceValue in Enum.GetValues(typeof(PieceInfo.Faces)))
            {
                var face = (PieceInfo.Faces)faceValue;
                if (face == PieceInfo.Faces.None)
                {
                    continue;
                }
                
                FacesColorInfo.Add(face, colors[i]);
                i++;
            }
        }

        private PlayerSave LoadLastPlayerSave()
        {
            if (!PlayerPrefs.HasKey(PlayerSavePrefsString))
            {
                return null;
            }

            return JsonConvert.DeserializeObject<PlayerSave>(PlayerPrefs.GetString(PlayerSavePrefsString));
        }

        public bool HasSaveFile()
        {
            return PlayerPrefs.HasKey(PlayerSavePrefsString);
        }

        [Serializable]
        [DataContract]
        public class PlayerSave
        {
            [DataMember(Name = "Cubes")]
            public List<string> CubesData;
            [DataMember(Name = "GameMode")]
            public GameMode GameMode;
            [DataMember(Name = "TimeSpend")]
            public int TimeSpend;
            [DataMember(Name = "MovesCount")]
            public int MovesCount;
            [DataMember(Name = " ShowMoves")] 
            public bool ShowMoves;
            [DataMember(Name = " ShowTimer")] 
            public bool ShowTimer;
        }
    }
}