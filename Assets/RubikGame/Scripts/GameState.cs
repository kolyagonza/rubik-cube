using System;

namespace RubikGame
{
    public class GameStateResolver
    {
        public enum GameState
        {
            None,
            Menu,
            Game,
            Pause,
            Won
        }

        public event Action<GameState> GameStateChanged;

        public GameState CurrentState
        {
            get => currentState;
            set
            {
                if (currentState != value)
                {
                    currentState = value;
                    GameStateChanged?.Invoke(currentState);
                }
            }
        }
        private GameState currentState;
    }
}