﻿using System;
using Navigation;
using UnityEngine;
using Zenject;

namespace RubikGame
{
    public class GameApplication : MonoBehaviour
    {
        private INavigation navigation;
        private PlayerModel playerModel;
        
        [Inject]
        private void Init(INavigation navigation, PlayerModel playerModel)
        {
            this.navigation = navigation;
            this.playerModel = playerModel;
        }

        private void Start()
        {
            playerModel.Load();
            navigation.Push<MainScreen>();
        }
    }
}