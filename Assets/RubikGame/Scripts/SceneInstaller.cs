using Navigation;
using RubikGame;
using RubikGame.Core;
using UnityEngine;
using Zenject;

public class SceneInstaller : MonoInstaller
{
    [SerializeField]
    private ScreensContainer screensContainer;

    [SerializeField] 
    private SettingsContainer settingsContainer;

    [SerializeField] 
    private Navigation.Navigation navigation;

    [SerializeField]
    private Transform cubesTargetContainer;
    
    public override void InstallBindings()
    {
        Container.BindInstance(screensContainer);
        Container.BindInstance<INavigation>(navigation);
        Container.Bind<PlayerModel>().AsSingle();
        Container.Bind<CubeGenerator>().AsSingle().WithArguments(cubesTargetContainer);
        Container.Bind<CubeProcessor>().AsSingle();
        Container.Bind<CubeMover>().FromNewComponentOnNewGameObject().AsSingle();
        Container.Bind<GameStateResolver>().AsSingle();
        Container.Bind<WinConditionChecker>().AsSingle();
        
        InstallFactories();
    }

    private void InstallFactories()
    {
        Container.BindInstance(settingsContainer.CubePrefab);
        Container.BindFactory<Transform, Vector3, PieceInfo, Cube, Cube.Factory>().FromFactory<Cube.ConcreteFactory>();
        
        Container.BindFactory<GameMode, GameModeButton, GameModeButton.Factory>()
            .FromComponentInNewPrefab (settingsContainer.GameModeButtonSettings.ButtonPrefab);
    }
}
