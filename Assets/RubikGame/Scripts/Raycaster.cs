using System;
using System.Collections.Generic;
using System.Linq;
using RubikGame.Core;
using UnityEngine;
using UnityEngine.EventSystems;
using Zenject;

public class Raycaster : MonoBehaviour
{
    private enum State
    {
        None,
        RotateCamera,
        RotateCube
    }
    
    private Camera targetCamera;
    private List<Cube> currentCubes;
    private CubeProcessor cubeProcessor;
    private CubeGenerator cubeGenerator;
    private Cube currentHoveredCube;
    private Vector3 rotationMiddlePoint;
    private Vector2 startMousePosition;
    private State state;
    private CubeMover cubeMover;


    [Inject]
    private void Init(CubeProcessor cubeProcessor, CubeGenerator cubeGenerator, CubeMover cubeMover)
    {
        this.cubeProcessor = cubeProcessor;
        this.cubeGenerator = cubeGenerator;
        this.cubeMover = cubeMover;
    }
    
    private void Awake()
    {
        targetCamera = Camera.main;
        currentCubes = new List<Cube>();
    }

    // Update is called once per frame
    private void Update()
    {
        if (RaycastUI().Count > 0)
        {
            return;
        }
        
        UpdateHoverState();
        UpdateMouseDownState();
        UpdateDeltaState();
        UpdateMouseUpState();
    }

    private void UpdateHoverState()
    {
        if (!Input.anyKey && state == State.None)
        {
            if (RaycastMousePosition(out var cube))
            {
                if (currentHoveredCube != cube)
                {
                    TryDeselectCurrentCubes();
                    currentCubes = cubeProcessor.GetMovableCubes(cube.Index).ToList();
                    foreach (var selectedCube in currentCubes)
                    {
                        selectedCube.OnPointerEnter();
                    }

                    currentHoveredCube = cube;
                }
            }
            else
            {
                TryDeselectCurrentCubes();
            }
        }
    }

    private bool RaycastMousePosition(out Cube cube)
    {
        cube = null;
        var ray = targetCamera.ScreenPointToRay(Input.mousePosition);

        if (Physics.Raycast(ray, out var hit))
        {
            if (hit.collider != null)
            {
                cube = hit.collider.gameObject.GetComponent<Cube>();
                return true;
            }
        }

        return false;
    }

    private void TryDeselectCurrentCubes()
    {
        if (currentCubes.Count > 0)
        {
            currentCubes.ForEach(cube => cube.OnPointerExit());
            currentCubes.Clear();
        }
    }

    private void UpdateMouseDownState()
    {
        if (Input.GetMouseButtonDown(0))
        {
            if (RaycastMousePosition(out var cube))
            {
                TryDeselectCurrentCubes();
                startMousePosition = Input.mousePosition;
                state = State.RotateCube;
            }
            else if (RaycastUI().Count > 0)
            {
                state = State.None;
            }
            else
            {
                state = State.RotateCamera;
            }
        }
    }
    
    private List<RaycastResult> RaycastUI()
    {
        var pointerData = new PointerEventData(EventSystem.current) {
            position = Input.mousePosition
        };
        
        var raycastResults = new List<RaycastResult>();
        EventSystem.current.RaycastAll(pointerData, raycastResults);
        return raycastResults;
    }

    private void UpdateDeltaState()
    {
        if (Input.GetMouseButton(0) && state == State.RotateCamera)
        {
            cubeGenerator.CubesContainer.Rotate(Input.GetAxis("Mouse Y") * 70 * Time.deltaTime, 
                -Input.GetAxis("Mouse X") * 70 * Time.deltaTime, 0, Space.World);
        }
    }

    private void UpdateMouseUpState()
    {
        if (Input.GetMouseButtonUp(0))
        {
            switch (state)
            {
                case State.RotateCube:
                {
                    ICommand command;
                    var xDelta = Input.mousePosition.x - startMousePosition.x;
                    var yDelta = Input.mousePosition.y - startMousePosition.y;
                    if (Math.Abs(xDelta) > Math.Abs(yDelta))
                    {
                        if (xDelta > 0)
                        {
                            command =
                                new HorizontalCounterClockwiseRotateCommand(cubeProcessor, cubeGenerator.CubesContainer, currentHoveredCube.Index);
                        }
                        else
                        {
                            command =
                                new HorizontalClockwiseRotateCommand(cubeProcessor, cubeGenerator.CubesContainer, currentHoveredCube.Index);
                        }
                        
                    }
                    else
                    {
                        if (yDelta > 0)
                        {
                            command =
                                new VerticalCounterClockwiseRotateCommand(cubeProcessor, cubeGenerator.CubesContainer, currentHoveredCube.Index);
                        }
                        else
                        {
                            command =
                                new VerticalClockwiseRotateCommand(cubeProcessor, cubeGenerator.CubesContainer, currentHoveredCube.Index);
                        }
                    }

                    command.CommandDone += OnCommandDone;
                    cubeMover.AddCommand(command);
                    break;
                }
                case State.None:
                case State.RotateCamera:
                default:
                    state = State.None;
                    break;
            }
        }
    }

    private void OnCommandDone(ICommand command)
    {
        command.CommandDone -= OnCommandDone;
        TryDeselectCurrentCubes();
        state = State.None;
    }
}
