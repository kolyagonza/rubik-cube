﻿using System;
using System.Collections.Generic;
using UnityEngine;
using Random = UnityEngine.Random;

namespace RubikGame
{
    public static class Utils
    {
        private static readonly List<Color> defaultColors = new List<Color>
        {
            Color.black, Color.blue, Color.cyan, Color.gray, Color.green, Color.magenta, Color.red, Color.white, Color.yellow
        };
        
        public static List<Color> GetRandomColors(int n)
        {
            var colors = new List<Color>();
            var usedColors = new List<int>();
            for (var i = 0; i < n; i++)
            {
                var nextColorIndex = Random.Range(0, defaultColors.Count);
                while (usedColors.Contains(nextColorIndex))
                {
                    nextColorIndex = Random.Range(0, defaultColors.Count);
                }
                
                usedColors.Add(nextColorIndex);
                colors.Add(defaultColors[nextColorIndex]);
            }

            return colors;
        }
    }
}